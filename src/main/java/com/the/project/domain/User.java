package com.the.project.domain;

import com.the.project.enums.UserRole;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Column(name = "login")
    private String login;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "age")
    private int age;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private UserRole userRole;

    @Column(name = "count_cigarette")
    private double countCigarette;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "friends", joinColumns = {
            @JoinColumn(name = "user_id", nullable = false, updatable = true)
    }, inverseJoinColumns = { @JoinColumn(name = "friend_id", nullable = false, updatable = true) })
    private List<User> friends;
    @Column(name = "eggs")
    private int eggs;
    @ManyToOne
    @JoinColumn(name = "company_id", nullable = true, updatable = true)
    private Company company;
    @Column(name = "last_smoke_time")
    private Date lastSmokeTime;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "order_action", joinColumns = {
            @JoinColumn(name = "user_id", nullable = false, updatable = true)
    }, inverseJoinColumns = { @JoinColumn(name = "action_id", nullable = false, updatable = true) })
    private List<Action> actions;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public double getCountCigarette() {
        return countCigarette;
    }

    public void setCountCigarette(double countCigarette) {
        this.countCigarette = countCigarette;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public int getEggs() {
        return eggs;
    }

    public void setEggs(int eggs) {
        this.eggs = eggs;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getLastSmokeTime() {
        return lastSmokeTime;
    }

    public void setLastSmokeTime(Date lastSmokeTime) {
        this.lastSmokeTime = lastSmokeTime;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }
}
