package com.the.project.enums;

public enum ConfirmStatus {

    CONFIRM,
    DECLINE,
    WAITING

}
