package com.the.project.enums;

public enum UserRole {

    User,
    Company,
    Admin

}
