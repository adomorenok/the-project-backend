package com.the.project.resource;

import com.the.project.domain.Action;
import com.the.project.repository.ActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Path;

@Component
@Path("action")
@Transactional
public class ActionResource extends BaseResource<Action> {

    @Autowired
    public void setRepository(ActionRepository repository) {
        super.setrepository(repository);
    }

}
