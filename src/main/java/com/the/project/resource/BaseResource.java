package com.the.project.resource;

import com.the.project.domain.BaseEntity;
import com.the.project.dto.ListDTO;
import com.the.project.repository.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional
public abstract class BaseResource<T extends BaseEntity> {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected BaseRepository<T> repository;

    @GET
    public Response findAll(@QueryParam("page") int page, @QueryParam("size") int size) {
            logger.debug("page: {}, to: {}", page, size);

        ListDTO<T> result = new ListDTO<T>();
        result.setSize(size);
        result.setCurrent(page);

        if (size > 0) {
            addDataToList(repository.findAll(new PageRequest(page, size)), result);
        } else {
            addDataToList(repository.findAll(), result);
        }

        return Response.ok(result).build();
    }

    @GET
    @Path("{id}")
    public Response findById(@PathParam("id") Long id) {
        T t = repository.findOne(id);
        if (t == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return Response.ok(oneToDto(t)).build();
    }

    @POST
    public Response create(T domain) {
        domain.setId(null);

        T t = repository.save(dtoToEntity(domain));
        return Response.ok(oneToDto(t)).build();
    }

    @PUT
    @Path("{id}")
    public Response update(@PathParam("id") Long id, T domain) {
        if(!repository.exists(id)) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        domain.setId(id);

        T t = repository.save(dtoToEntity(domain));
        return Response.ok(oneToDto(t)).build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteById(@PathParam("id") Long id) {
        repository.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    public void setrepository(BaseRepository<T> repository) {
        this.repository = repository;
    }

    protected void addDataToList(Page page, ListDTO dto) {
        dto.setData(listToDto(page.getContent()));
        dto.setTotal((int) page.getTotalElements());
        dto.setNext(page.hasNextPage());
        dto.setPrev(page.hasPreviousPage());
        dto.calculationPages();
    }

    protected void addDataToList(List list, ListDTO dto) {
        dto.setData(listToDto(list));
        dto.setTotal(dto.getData().size());
        dto.setNext(false);
        dto.setPrev(false);
        dto.calculationPages();
    }

    protected T oneToDto(T t) {
        return t;
    }

    protected List<T> listToDto(List<T> ts) {
        List<T> newTs = new ArrayList<>();
        for (T t : ts) {
            newTs.add(dtoToEntity(t));
        }

        return newTs;
    }

    protected T dtoToEntity(T dto) {
        return dto;
    }

}
