package com.the.project.resource;

import com.the.project.domain.Event;
import com.the.project.domain.User;
import com.the.project.repository.EventRepository;
import com.the.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Path;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("event")
@Transactional
public class EventResource extends BaseResource<Event> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public void setRepository(EventRepository repository) {
        super.setrepository(repository);
    }

    @Override
    protected Event oneToDto(Event event) {
        Event newEvent = new Event();
        newEvent.setId(event.getId());
        newEvent.setName(event.getName());
        newEvent.setDate(event.getDate());

        List<User> users = new ArrayList<>();
        for (User u : event.getUsers()) {
            User user = new User();
            user.setId(u.getId());
            user.setLogin(u.getLogin());
            user.setEmail(u.getEmail());
        }
        newEvent.setUsers(users);

        return newEvent;
    }

    @Override
    protected Event dtoToEntity(Event dto) {
        List<User> users = new ArrayList<>();
        for (User u : dto.getUsers()) {
            users.add(userRepository.findOne(u.getId()));
        }
        dto.setUsers(users);

        return dto;
    }
}