package com.the.project.resource;

import com.the.project.domain.Action;
import com.the.project.domain.User;
import com.the.project.dto.ListDTO;
import com.the.project.dto.PurchasedActionDTO;
import com.the.project.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("user")
@Transactional
public class UserResource extends BaseResource<User> {

    @Autowired
    public void setRepository(UserRepository repository) {
        super.setrepository(repository);
    }

    @POST
    @Path("/login")
    public Response login(User user) {
        List<User> users = ((UserRepository)repository).findByEmail(user.getEmail());
        if (users.isEmpty()) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        if (!users.get(0).getPassword().equals(user.getPassword())) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(oneToDto(users.get(0))).build();
    }

    @POST
    @Path("/by-email")
    public Response byEmail(User user) {
        List<User> users = ((UserRepository)repository).findByEmail(user.getEmail());
        if (users.isEmpty()) {
            return Response.status(Response.Status.CONFLICT).build();
        }

        return Response.ok(oneToDto(users.get(0))).build();
    }

    @GET
    @Path("/get-purchased-actions")
    @Transactional
    public Response getPurchasedActions(@QueryParam("id") long id, @QueryParam("page") int page, @QueryParam("size") int size) {
        ListDTO<PurchasedActionDTO> result = new ListDTO<PurchasedActionDTO>();
        List<PurchasedActionDTO> list = new ArrayList<>();
        result.setSize(size);
        result.setCurrent(page);

        User user = ((UserRepository)repository).findOne(id);
        if (user == null) {
            return Response.status(Response.Status.CONFLICT).build();
        }

//        Integer from = page * size;
//        Integer to = (page + 1) * size;

        for (Action action: user.getActions()) {
            PurchasedActionDTO dto = new PurchasedActionDTO();
            dto.setName(action.getName());
            dto.setPersent(action.getPercent());
            dto.setCompany(action.getCompany());
            list.add(dto);
        }

        if (size > 0) {
            addDataToList(list, result);
        } else {
            addDataToList(list, result);
        }

        return Response.ok(result).build();
    }

}
