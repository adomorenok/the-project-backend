package com.the.project.resource;

import com.the.project.domain.Confirmation;
import com.the.project.repository.ConfirmationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Path;

@Component
@Path("confirmation")
@Transactional
public class ConfirmationResource extends BaseResource<Confirmation> {

    @Autowired
    public void setRepository(ConfirmationRepository repository) {
        super.setrepository(repository);
    }

}