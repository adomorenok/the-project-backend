package com.the.project.resource;


import com.the.project.domain.Company;
import com.the.project.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Path;

@Component
@Path("company")
@Transactional
public class CompanyResource extends BaseResource<Company> {

    @Autowired
    public void setRepository(CompanyRepository repository) {
        super.setrepository(repository);
    }

}
