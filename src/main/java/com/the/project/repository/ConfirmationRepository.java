package com.the.project.repository;

import com.the.project.domain.Confirmation;

public interface ConfirmationRepository extends BaseRepository<Confirmation> {
}
