package com.the.project.repository;

import com.the.project.domain.Company;

public interface CompanyRepository extends BaseRepository<Company> {
}
