package com.the.project.repository;

import com.the.project.domain.User;

import java.util.List;

public interface UserRepository extends BaseRepository<User> {

    List<User> findByEmail(String email);

}
