package com.the.project.repository;

import com.the.project.domain.Event;

public interface EventRepository extends BaseRepository<Event> {
}
