package com.the.project.repository;

import com.the.project.domain.Action;

public interface ActionRepository extends BaseRepository<Action> {
}
