package com.the.project.dto;

import com.the.project.domain.Company;

/**
 * Created by aliaksandr.marozau.
 */
public class PurchasedActionDTO {

    private String name;
    private double persent;
    private Company company;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPersent() {
        return persent;
    }

    public void setPersent(double persent) {
        this.persent = persent;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
