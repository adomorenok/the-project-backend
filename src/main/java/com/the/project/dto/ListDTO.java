package com.the.project.dto;

import java.util.List;

public class ListDTO<T> {


    private List<T> data;
    private String message;
    private Integer code;
    private boolean next;
    private boolean prev;
    private int total;
    private int size;
    private int current;
    private int[] pages;

    public ListDTO() {
    }

    public ListDTO(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public ListDTO(List<T> data) {
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isNext() {
        return next;
    }

    public void setNext(boolean next) {
        this.next = next;
    }

    public boolean isPrev() {
        return prev;
    }

    public void setPrev(boolean prev) {
        this.prev = prev;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int[] getPages() {
        return pages;
    }

    public void setPages(int[] pages) {
        this.pages = pages;
    }

    public void calculationPages() {
        int countPages = 0;

        if (size == 0) {
            this.pages = new int[1];
            this.pages[0] = 0;
        } else {
            if (total % this.size == 0) {
                countPages = (int) Math.ceil(((total + 0.0) - 1) / this.size);
            } else {
                countPages = (int) Math.ceil((total + 0.0) / this.size);
            }

            if (countPages > 0) {
                int start = Math.max(this.current - 3, 0) + 0;
                int finish = Math.min(start + 6, countPages) + 0;
                start = Math.max(finish - 6, 0) + 0;

                this.pages = new int[finish - start];
                int j = 0;
                for (int i = start; i < finish; i++) {
                    pages[j] = i;
                    j++;
                }
            }
        }
    }

}
