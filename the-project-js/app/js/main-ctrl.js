app.MainCtrl = function ($scope, $rootScope, $http, $location) {

    $rootScope.currentUser = undefined;

    $rootScope.loadCurrentUser = function(callback) {
        var email = $.cookie('email');

        if (email) {
            $http.post('/api/user/by-email', {email: email}).
                success(function(data, status, headers, config) {
                    $rootScope.currentUser = data;
                    if (callback) {
                        callback();
                    }
                }).error(function(data, status, headers, config) {
                    console.log('Error loading user');
                });
        }
    };

    $rootScope.loadCurrentUser();

    $rootScope.logout = function() {
        $.removeCookie('email');
        delete $rootScope.currentUser;
        $location.path("/home");
    };

    $rootScope.isLogin = function() {
        return $rootScope.currentUser ? true : false;
    };

    $rootScope.isAdmin = function() {
        return $rootScope.currentUser && $rootScope.currentUser.userRole == 'Admin';
    };

    $rootScope.isCompany = function() {
        return $rootScope.currentUser && $rootScope.currentUser.userRole == 'Company';
    };

    $rootScope.isUser = function() {
        return $rootScope.currentUser && $rootScope.currentUser.userRole == 'User';
    };


    $rootScope.showMessage = function(text, type) {
        noty({
            text        : text,
            type        : type,
            dismissQueue: true,
            layout      : 'topRight',
            closeWith   : [],
            theme       : 'relax',
            maxVisible  : 10,
            timeout: 5000,
            animation   : {
                open  : 'animated bounceInLeft',
                close : 'animated bounceOutLeft',
                easing: 'swing',
                speed : 500,
                width : 400
            }
        });
    };

    $rootScope.showAlert = function(text) {
        $rootScope.showMessage(text, 'alert');
    };

    $rootScope.showSuccess = function(text) {
        $rootScope.showMessage(text, 'success');
    };
    $rootScope.showError = function(text) {
        $rootScope.showMessage(text, 'error');
    };

    $rootScope.showWarning = function(text) {
        $rootScope.showMessage(text, 'warning');
    };

    $rootScope.showInformation = function(text) {
        $rootScope.showMessage(text, 'information');
    };

    $rootScope.showConfirm = function(text) {
        $rootScope.showMessage(text, 'confirm');
    };

}
