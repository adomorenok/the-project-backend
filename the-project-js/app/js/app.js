var app = angular.module('the-project', ['autocomplete', 'ngRoute', 'ngAnimate', 'ngTouch', 'route-segment', 'view-segment', 'ui.bootstrap', 'ui.router']);
// configuring angular
app.config(function ($routeSegmentProvider, $routeProvider, $httpProvider, $provide) {

    // Configuring provider options

    $routeSegmentProvider.options.autoLoadTemplates = true;

    // Setting routes. This consists of two parts:
    // 1. `when` is similar to vanilla $route `when` but takes segment name instead of params hash
    // 2. traversing through segment tree to set it up

    $routeSegmentProvider
        .when('/login', 'login')

        .segment('login', {
            templateUrl: 'public/template/login.html',
            controller: app.LoginCtrl});

    $routeSegmentProvider
        .when('/registration', 'registration')

        .segment('registration', {
            templateUrl: 'public/template/registration.html',
            controller: app.RegistrationCtrl});

    $routeSegmentProvider
        .when('/profile', 'profile')

        .segment('profile', {
            templateUrl: 'public/template/profile.html',
            controller: app.ProfileCtrl});

    $routeSegmentProvider
        .when('/home', 'home')

        .segment('home', {
            templateUrl: 'public/template/home.html',
            controller: app.HomeCtrl});

    $routeSegmentProvider
        .when('/users', 'users')

        .segment('users', {
            templateUrl: 'public/template/users.html',
            controller: app.UsersCtrl});

    $routeSegmentProvider
        .when('/company', 'company')

        .segment('company', {
            templateUrl: 'public/template/company.html',
            controller: app.CompanyCtrl});

    $routeSegmentProvider
        .when('/actions', 'actions')

        .segment('actions', {
            templateUrl: 'public/template/actions.html',
            controller: app.ActionsCtrl});

    $routeProvider.otherwise({redirectTo: '/actions'});

});
