app.RegistrationCtrl = function ($scope, $http, $rootScope, $location) {

    $scope.user = {};

    $scope.login = function() {
        $scope.user.userRole = 'User';
        $http.post('/api/user', $scope.user).
            success(function(data, status, headers, config) {
                $.cookie('email', $scope.user.email);
                $rootScope.showSuccess("Login success");
                $rootScope.loadCurrentUser();
                $location.path("/home");
            }).error(function(data, status, headers, config) {
                $rootScope.showError("Bad request")
            });
    };


}