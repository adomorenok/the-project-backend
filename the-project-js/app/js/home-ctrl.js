app.HomeCtrl = function ($scope, $rootScope, $http) {
    $scope.USER_TABLE = 'purchased_actions';

    $scope.init = []; // init function
    $scope.size = []; // count elements in page
    $scope.next = []; // number is next page
    $scope.current = []; // number is current page
    $scope.prev = []; // number is previous page
    $scope.pages = []; // total pages
    $scope.count = []; // count total elements
    $scope.data = []; // list data

    $scope.setData = function(table, data) {
        $scope.size[table] = data.size;
        $scope.data[table] = data.data;
        $scope.next[table] = data.next;
        $scope.current[table] = data.current;
        $scope.count[table] = data.total;
        $scope.prev[table] = data.prev;
        $scope.pages[table] = data.pages;
    };

    // get data for current page
    $scope.getPage = function(page, tableName) {
        if ($scope.current[tableName] != page) {
            $scope.current[tableName] = page;
            $scope.init[tableName]();
        }
    };

    $scope.getPageInfo = function(table) {
        if ($scope.count[table]) {
            return 'From ' +
                ($scope.count[table] != 0 ? ($scope.current[table] * $scope.size[table] + 1) : 0) +
                ' to ' +
                ($scope.count[table] < ($scope.current[table] * $scope.size[table] + $scope.size[table]) ? $scope.count[table] : ($scope.current[table] * $scope.size[table] + $scope.size[table])) +
                ' of ' + $scope.count[table];
        }

        return 'No Records';
    };

    // get data for next page
    $scope.getNextPage = function(tableName) {
        if ($scope.current[tableName] != $scope.pages[tableName][$scope.pages[tableName].length - 1]) {
            $scope.current[tableName]++;
            $scope.init[tableName]();
        }
    };

    // get data for previous page
    $scope.getPrevPage = function(tableName) {
        if ($scope.current[tableName] != 0) {
            $scope.current[tableName]--;
            $scope.init[tableName]();
        }
    };

    $scope.setData($scope.USER_TABLE, {
        size : 10,
        current : 0
    });

    $scope.init[$scope.USER_TABLE] = function() {
        $http.get('/api/user/get-purchased-actions?id='+ $rootScope.currentUser.id + '&page=' + $scope.current[$scope.USER_TABLE] + '&size=' + $scope.size[$scope.USER_TABLE])
            .success(function(data, status, headers, config) {
                $scope.setData($scope.USER_TABLE, data);
            }).error(function(data, status, headers, config) {
                $scope.setData($scope.USER_TABLE, {
                    size : 10,
                    current : 0
                });
            });
    };

    $scope.init[$scope.USER_TABLE]();



    //DO NOT DELETE
    $scope.rouletteWebsocket = new WebSocket('ws://localhost:8080/websocket');
    $scope.email = '';

    $scope.rouletteWebsocket.onerror = function(event) {
        $scope.rouletteOnError(event);
    };

    $scope.rouletteWebsocket.onopen = function(event) {
        $scope.rouletteOnOpen(event);
    };

    $scope.rouletteWebsocket.onmessage = function(event) {
        $scope.rouletteOnMessage(event);
    };

    $scope.rouletteOnMessage = function(event) {
        document.getElementById('messages').innerHTML
            += '<br />' + event.data;
    };

    $scope.rouletteOnOpen = function(event) {
        document.getElementById('messages').innerHTML
            = 'Connection established';
    };

    $scope.rouletteOnError = function(event) {
        alert(event.data);
    };

    $scope.start = function() {
        $scope.rouletteWebsocket.send($scope.email);
        return false;
    };

    //-----------------------------------------------------------------
};