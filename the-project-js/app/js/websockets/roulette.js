app.RouletteCtrl = function ($scope, $rootScope, $routeSegment, $sce) {
    $scope.rouletteWebsocket = new WebSocket('ws://localhost:8080/websocket');
    $scope.email = '';

    $scope.rouletteWebsocket.onerror = function(event) {
        $scope.rouletteOnError(event);
    };

    $scope.rouletteWebsocket.onopen = function(event) {
        $scope.rouletteOnOpen(event);
    };

    $scope.rouletteWebsocket.onmessage = function(event) {
        $scope.rouletteOnMessage(event);
    };

    $scope.rouletteOnMessage = function(event) {
        document.getElementById('messages').innerHTML
            += '<br />' + event.data;
    };

    $scope.rouletteOnOpen = function(event) {
        document.getElementById('messages').innerHTML
            = 'Connection established';
    };

    $scope.rouletteOnError = function(event) {
        alert(event.data);
    };

    $scope.start = function() {
        $scope.rouletteWebsocket.send($scope.email);
        return false;
    };
};