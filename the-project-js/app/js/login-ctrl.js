app.LoginCtrl = function ($scope, $http, $rootScope, $location) {

    $scope.user = {};

    $scope.login = function () {
        $http.post('/api/user/login', $scope.user).
            success(function (data, status, headers, config) {
                $.cookie('email', $scope.user.email);
                $rootScope.showSuccess("Login success");
                $rootScope.loadCurrentUser(
                    function () {
                        switch ($rootScope.currentUser.userRole) {
                            case 'Admin':
                                $location.path("/users");
                                break;
                            case 'User':
                                $location.path("/home");
                                break;
                            case 'Company':
                                $location.path("/actions");
                                break;
                            default:
                                $location.path("/actions");
                        }
                    }
                );
            }).error(function (data, status, headers, config) {
                $rootScope.showError("Bad credentials")
            });
    };
};