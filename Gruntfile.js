module.exports = function(grunt) {

    var files = {
        js: [
			//'<%= dirs.src %>/vendor/*.js',
            '<%= dirs.src %>/app/js/app.js',
            '<%= dirs.src %>/app/js/jarviswidgets.js',
            '<%= dirs.src %>/app/js/*-ctrl.js'
        ],
        vendor: [
            '<%= dirs.src %>/vendor/js/jquery-2.0.2.min.js',
            '<%= dirs.src %>/vendor/js/jquery.base64.min.js',
            '<%= dirs.src %>/vendor/js/jquery-ui-1.10.3.min.js',
            '<%= dirs.src %>/vendor/js/angular.js',
            '<%= dirs.src %>/vendor/js/angular-animate.js',
            '<%= dirs.src %>/vendor/js/angular-touch.js',
            '<%= dirs.src %>/vendor/js/angular-resource.js',
            '<%= dirs.src %>/vendor/js/angular-route.js',
            '<%= dirs.src %>/vendor/js/angular-route-segment.js',
            '<%= dirs.src %>/vendor/js/angular-sanitize.js',
            '<%= dirs.src %>/vendor/js/autocomplete.js',
            '<%= dirs.src %>/vendor/js/bootstrap.min.js',
            '<%= dirs.src %>/vendor/js/ui-bootstrap-tpls-0.10.0.min.js',
            '<%= dirs.src %>/vendor/js/underscore.js',
            '<%= dirs.src %>/vendor/js/SmartNotification.min.js',
            '<%= dirs.src %>/vendor/js/jarvis.widget.js',
            '<%= dirs.src %>/vendor/js/jquery.easy-pie-chart.min.js',
            '<%= dirs.src %>/vendor/js/jquery.sparkline.min.js',
            '<%= dirs.src %>/vendor/js/jquery.validate.min.js',
            '<%= dirs.src %>/vendor/js/jquery.maskedinput.min.js',
            '<%= dirs.src %>/vendor/js/select2.min.js',
            '<%= dirs.src %>/vendor/js/jquery.mb.browser.min.js',
            '<%= dirs.src %>/vendor/js/jquery.cookie.js',
            '<%= dirs.src %>/vendor/js/jquery.base64.min.js',
            '<%= dirs.src %>/vendor/js/smartclick.js',
            '<%= dirs.src %>/vendor/js/superbox.min.js',
            '<%= dirs.src %>/vendor/js/date.format.js',
            '<%= dirs.src %>/vendor/js/typeahead.min.js',
            '<%= dirs.src %>/vendor/js/jquery.dataTables-cust.min.js',
            '<%= dirs.src %>/vendor/js/jquery.flot.cust.js',
            '<%= dirs.src %>/vendor/js/jquery.numeric.js',
            '<%= dirs.src %>/vendor/js/ColReorder.min.js',
            '<%= dirs.src %>/vendor/js/jquery.flot.resize.min.js',
            '<%= dirs.src %>/vendor/js/jquery.flot.fillbetween.min.js',
            '<%= dirs.src %>/vendor/js/FixedColumns.min.js',
            '<%= dirs.src %>/vendor/js/jquery.flot.orderBar.min.js',
            '<%= dirs.src %>/vendor/js/ColVis.min.js',
            '<%= dirs.src %>/vendor/js/jquery.flot.pie.min.js',
            '<%= dirs.src %>/vendor/js/ZeroClipboard.js',
            '<%= dirs.src %>/vendor/js/jquery.flot.tooltip.min.js',
            '<%= dirs.src %>/vendor/js/TableTools.min.js',
            '<%= dirs.src %>/vendor/js/DT_bootstrap.js',
            '<%= dirs.src %>/vendor/js/jquery-form.min.js',
            '<%= dirs.src %>/vendor/js/jquery.blueimp-gallery.min.js',
            '<%= dirs.src %>/vendor/js/bootstrap-image-gallery.min.js',
            '<%= dirs.src %>/vendor/js/jquery.PrintArea.js',
            '<%= dirs.src %>/vendor/js/ui.dropdownchecklist-1.4-min.js',
            '<%= dirs.src %>/vendor/js/ui-router.js',
            '<%= dirs.src %>/vendor/js/md5.js',
            '<%= dirs.src %>/vendor/js/tableExport.js',
            '<%= dirs.src %>/vendor/js/sprintf.js',
            '<%= dirs.src %>/vendor/js/accounting.js',
            '<%= dirs.src %>/vendor/js/jquery.noty.packaged.min.js'
        ]
    };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        dirs: {
			src: './the-project-js',
            build: './src/main/webapp/public'
        },

        concat: {
            options: {
                separator: ';',
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd HH:MM") %> */'
            },
            vendor: {
                src: files.vendor,
                dest: '<%= dirs.build %>/js/vendor.js'
            },
            js: {
                src: files.js,
                dest: '<%= dirs.build %>/js/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd HH:MM") %> */'
            },
            js: {
                files: {
                    '<%= dirs.build %>/js/<%= pkg.name %>.min.js': ['<%= dirs.build %>/js/<%= pkg.name %>.js']
                }
            },
            vendor: {
                files: {
                    '<%= dirs.build %>/js/vendor.min.js': ['<%= dirs.build %>/js/vendor.js']
                }
            }
        },
        jshint: {
            options: {
                smarttabs: true,
                '-W030': false
            },
            js: files.js
        },
        copy: {
            img: {
                expand: true,
                cwd: '<%= dirs.src %>/app/img/',
                src: '**',
                dest: '<%= dirs.build %>/img/'
            },
			template: {
				expand: true,
				cwd: '<%= dirs.src %>/app/template/',
				src: '**',
				dest: '<%= dirs.build %>/template/'
			},
			font: {
				expand: true,
				cwd: '<%= dirs.src %>/vendor/font/',
				src: '**',
				dest: '<%= dirs.build %>/fonts/'
			},
			index: {
				expand: true,
				cwd: '<%= dirs.src %>/app/',
				src: 'index.html',
				dest: '<%= dirs.build %>/'
			}
			
        },
		cssmin: {
			combine: {
				files: {
					'<%= dirs.build %>/css/<%= pkg.name %>.min.css': ['<%= dirs.src %>/vendor/css/**.css', '<%= dirs.src %>/app/css/**.css']
				}
			}
		},
        watch: {
            js: {
                files: files.js,
                tasks: ['concat:js']
            },
            vendor: {
                files: files.vendor,
                tasks: ['concat:vendor']
            },
            img: {
                files: ['<%= dirs.src %>/app/img/**'],
                tasks: ['copy:img']
            },
            template: {
                files: ['<%= dirs.src %>/app/template/**'],
                tasks: ['copy:template']
            },
            font: {
                files: ['<%= dirs.src %>/vendor/font/**'],
                tasks: ['copy:font']
            },
            index: {
                files: ['<%= dirs.src %>/app/index.html'],
                tasks: ['copy:index']
            },
			cssmin: {
				files: ['<%= dirs.src %>/vendor/css/**.css', '<%= dirs.src %>/app/css/**.css'],
                tasks: ['cssmin:combine']
			}
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['concat', /*'jshint',*/ 'uglify', 'copy', 'cssmin', 'watch']);
    grunt.registerTask('build', ['concat', 'jshint', 'uglify', 'copy', 'cssmin']);
    /*grunt.registerTask('validate', ['jshint']);*/
};